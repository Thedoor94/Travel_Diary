package com.traveldiary.porta.traveldiary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private String title;
    private double longitude;
    private double latitude;
    private String locationStart;
    private String locationClose;
    private String illustration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        title=getIntent().getStringExtra("keyName");
        SharedPreferences prefs = getSharedPreferences("appData", MODE_PRIVATE);
        String restoredText = prefs.getString(title, null);
        if (restoredText != null) {
            try {
                JSONObject jObj = new JSONObject(restoredText);
                locationStart = jObj.getString("locationStart");
                locationClose = jObj.getString("locationClose");

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON");
            }

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Geocoder coder = new Geocoder(this);
        SharedPreferences prefs = getSharedPreferences("appData", MODE_PRIVATE);
        String restoredText = prefs.getString(title, null);
        if (restoredText != null) {
            try {
                JSONObject jObj = new JSONObject(restoredText);
                locationStart = jObj.getString("locationStart");
                locationClose = jObj.getString("locationClose");
                illustration = jObj.getString("illustration");

                ArrayList<Address> adressesStartLocation = (ArrayList<Address>) coder.getFromLocationName(locationStart, 50);
                for (Address add : adressesStartLocation) {

                    longitude = add.getLongitude();
                    latitude = add.getLatitude();
                    LatLng coordinates = new LatLng(latitude, longitude);
                    Marker marker = mMap.addMarker(new MarkerOptions().position(coordinates).title(locationStart).snippet(illustration));
                    marker.showInfoWindow();
                    marker.hideInfoWindow();
                }
                ArrayList<Address> adressesCloseLocation = (ArrayList<Address>) coder.getFromLocationName(locationClose, 50);
                for (Address add : adressesCloseLocation) {

                    longitude = add.getLongitude();
                    latitude = add.getLatitude();
                    LatLng coordinates = new LatLng(latitude, longitude);
                    Marker marker = mMap.addMarker(new MarkerOptions().position(coordinates).title(locationClose).snippet(illustration));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 6));
                    marker.showInfoWindow();
                    marker.hideInfoWindow();

                }

                JSONArray jArr = jObj.getJSONArray("travel");
                for (int i = 0; i < jArr.length(); i++) {
                    JSONObject obj = jArr.getJSONObject(i);
                    ArrayList<Address> adressesStop = (ArrayList<Address>) coder.getFromLocationName(obj.getString("address"), 50);

                    if (adressesStop.size()>0) {
                        for (Address add : adressesStop) {

                            longitude = add.getLongitude();
                            latitude = add.getLatitude();
                            LatLng coordinates = new LatLng(latitude, longitude);
                            mMap.addMarker(new MarkerOptions().position(coordinates).title(obj.getString("address")).snippet(obj.getString("shortDescription"))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                        }
                    } else{
                        longitude = Double.parseDouble(obj.getString("longitude"));
                        latitude = Double.parseDouble(obj.getString("latitude"));
                        LatLng coordinates = new LatLng(latitude, longitude);
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(this, Locale.getDefault());
                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        String city = addresses.get(0).getLocality();
                        mMap.addMarker(new MarkerOptions().position(coordinates).title(city).snippet(obj.getString("shortDescription"))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

                        }

                    }

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON");
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MapsActivity.this, Homepage.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_window) {
            Intent intent = new Intent(MapsActivity.this, AddNewStop.class);
            intent.putExtra("keyName",title);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.homepage) {
            Intent intent = new Intent(MapsActivity.this, Homepage.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(MapsActivity.this, AddNewTravel.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
