package com.traveldiary.porta.traveldiary;

import java.util.List;

public class Travel {
    private String name;
    private String illustration;
    private String locationStart;
    private String startDate;
    private String locationClose;
    private String closeDate;
    private String image_path;


    private List<Stop> stopList;

    public String getImage_path() {return image_path;}

    public void setImage_path(String image_path) {this.image_path = image_path;}

    public String getIllustration() {
        return illustration;
    }

    public void setIllustration(String illustration) {
        this.illustration = illustration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationStart() {
        return locationStart;
    }

    public void setLocationStart(String locationStart) {
        this.locationStart = locationStart;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLocationClose() {
        return locationClose;
    }

    public void setLocationClose(String locationClose) {
        this.locationClose = locationClose;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public List<Stop> getStopList() {
        return stopList;
    }

    public void setStopList(List<Stop> stopList) {
        this.stopList = stopList;
    }


}
