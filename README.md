# Travel Diary
### App per Android
L'utente può utilizzare questa applicazione per memorizzare le informazioni sui propri viaggi.

Ogni viaggio sarà descritto da un nome, una breve descrizione, una data di inizio e la posizione,
una data di fine e la posizione, e un elenco di possibili posizioni intermedie.

Ogni posizione sarà descritta da un nome, una posizione (entrambi latitudine, longitudine e
indirizzo), e una descrizione breve opzionale.

Per ogni viaggio, una mappa mostra tutte le fermate effettuate.

È possibile aggiungere file multimediali per ogni viaggio (foto, video) e la condivisione dei viaggi sui social networks.