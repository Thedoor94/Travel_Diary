package com.traveldiary.porta.traveldiary;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;

public class AddNewTravel extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LocationListener {

    private LocationManager locationManager;
    private int REQUEST_CAMERA = 0;
    private Button btnSelect;
    private ImageView imageViewPhoto;
    private String userChoosenTask;
    private String imagePath;
    private EditText title;
    private EditText start;
    private EditText illustration;
    private EditText finish;
    private EditText dateStart;
    private EditText dateFinish;
    private boolean enable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_travel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    500, 1, this);
        } catch (SecurityException e) {
            Log.d("Errore Gps", e.toString());
        }
        btnSelect = (Button) findViewById(R.id.buttonPhoto);
        btnSelect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        title = (EditText) findViewById(R.id.editTextTitle);
        start = (EditText) findViewById(R.id.editTextStart);
        illustration = (EditText) findViewById(R.id.editTextIllustration);
        finish = (EditText) findViewById(R.id.editTextFinish);
        dateStart = (EditText) findViewById(R.id.editTextDateStart);
        dateFinish = (EditText) findViewById(R.id.editTextDateFinish);
        imageViewPhoto = (ImageView) findViewById(R.id.imageViewPhoto);

        start.setEnabled(true);
        enable=false;
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewTravel.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(AddNewTravel.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();

                }
                break;
        }
    }


    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);


        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        imagePath = destination.getAbsolutePath();

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageViewPhoto.setImageBitmap(thumbnail);

    }


    @Override
    public void onResume() {
        super.onResume();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 1, this);
        } catch (SecurityException e) {
            Log.d("Errore Gps", e.toString());
        }

    }

    @Override
    public void onLocationChanged(Location location) {

        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0 && enable==true) {
                String city = addresses.get(0).getLocality().toString();
                start.setText(city);
            }

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.travel_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_window) {

            ArrayList<Stop> stopList = new ArrayList<Stop>();

            Travel travel = new Travel();
            travel.setName(title.getText().toString());
            travel.setIllustration(illustration.getText().toString());
            travel.setLocationStart(start.getText().toString());
            travel.setLocationClose(finish.getText().toString());
            travel.setStartDate(dateStart.getText().toString());
            travel.setCloseDate(dateFinish.getText().toString());
            travel.setImage_path(imagePath);
            travel.setStopList(stopList);

            String str = JsonUtil.toJSon(travel);
            SharedPreferences.Editor editor = getSharedPreferences("appData", MODE_PRIVATE).edit();
            editor.putString( title.getText().toString(), str );
            editor.commit();

            Intent intent = new Intent(AddNewTravel.this, Homepage.class);
            startActivity(intent);

            return true;
        } else {

            if (enable==true) {
                enable=false;
                start.setEnabled(false);

            } else {
                start.setEnabled(true);
                enable = true;
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        500, 1, this);
            } catch (SecurityException e) {
                Log.d("Errore Gps", e.toString());
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.homepage) {
            Intent intent = new Intent(AddNewTravel.this, Homepage.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AddNewTravel.this, AddNewTravel.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

