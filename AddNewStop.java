package com.traveldiary.porta.traveldiary;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class AddNewStop extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,LocationListener {

    private EditText title;
    private EditText lat;
    private EditText lon;
    private EditText address;
    private EditText illustration;
    private String title_name;
    private LocationManager locationManager;
    private boolean enable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_stop);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        title_name=getIntent().getStringExtra("keyName");
        title = (EditText) findViewById(R.id.editTextTitle);
        lat = (EditText) findViewById(R.id.editTextLat);
        lon = (EditText) findViewById(R.id.editTextLon);
        address = (EditText) findViewById(R.id.editTextAddress);
        illustration = (EditText) findViewById(R.id.editTextIllustration);
        lat.setEnabled(false);
        lon.setEnabled(false);
        enable=true;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.travel_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_window) {

            ArrayList<Stop> stopList = new ArrayList<Stop>();

            Travel travel = new Travel();
            SharedPreferences prefs = getSharedPreferences("appData", MODE_PRIVATE);
            String restoredText = prefs.getString(title_name, null);
            try {
                JSONObject jObj = new JSONObject(restoredText);
                travel.setName(jObj.getString("name"));
                travel.setIllustration(jObj.getString("illustration"));
                travel.setLocationStart(jObj.getString("locationStart"));
                travel.setStartDate(jObj.getString("startDate"));
                travel.setLocationClose(jObj.getString("locationClose"));
                travel.setCloseDate(jObj.getString("closeDate"));
                travel.setImage_path(jObj.getString("imagePath"));
                travel.setStopList(stopList);
                JSONArray jArr = jObj.getJSONArray("travel");

                for (int i=0; i < jArr.length(); i++) {

                    JSONObject obj = jArr.getJSONObject(i);
                    Stop stop= new Stop();
                    stop.setName(obj.getString("name"));
                    stop.setAddress(obj.getString("address"));
                    stop.setLatitude(obj.getString("latitude"));
                    stop.setLongitude(obj.getString("longitude"));
                    stop.setShortDescription(obj.getString("shortDescription"));
                    stopList.add(stop);

                }

                Stop stop1= new Stop();
                stop1.setName(title.getText().toString());
                stop1.setAddress(address.getText().toString());
                stop1.setLatitude(lat.getText().toString());
                stop1.setLongitude(lon.getText().toString());
                stop1.setShortDescription(illustration.getText().toString());
                stopList.add(stop1);


            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON");
            }

            travel.setStopList(stopList);
            String str = JsonUtil.toJSon(travel);
            SharedPreferences.Editor editor = getSharedPreferences("appData", MODE_PRIVATE).edit();
            editor.remove(title_name);
            editor.putString(title_name, str );
            editor.commit();
            Intent intent = new Intent(AddNewStop.this, MapsActivity.class);
            intent.putExtra("keyName",title_name);
            startActivity(intent);

            return true;

        } else {
            if (enable==true) {
                enable=false;
                lat.setEnabled(false);
                lon.setEnabled(false);
            } else {
                lat.setEnabled(true);
                lon.setEnabled(true);
                enable = true;
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        500, 1, this);
            } catch (SecurityException e) {
                Log.d("Errore Gps", e.toString());
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.homepage) {
            Intent intent = new Intent(AddNewStop.this, Homepage.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AddNewStop.this, AddNewTravel.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 1, this);
        } catch (SecurityException e) {
            Log.d("Errore Gps", e.toString());
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        if (enable==false) {
            lat.setText("" + location.getLatitude());
            lon.setText("" + location.getLongitude());
        }

    }

    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }
}


