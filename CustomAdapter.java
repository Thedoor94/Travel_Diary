package com.traveldiary.porta.traveldiary;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class CustomAdapter extends BaseAdapter{
    String [] result;
    String [] result2;
    Context context;
    String [] imagePath;

    private static LayoutInflater inflater=null;
    public CustomAdapter(Homepage homepage, String[] titleList, String[] illustrationList , String[] images) {
        // TODO Auto-generated constructor stub
        result=titleList;
        result2= illustrationList;
        context=homepage;
        imagePath=images;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tvTitle;
        TextView tvIllustration;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.custom_list, null);
        holder.tvTitle =(TextView) rowView.findViewById(R.id.list_title);
        holder.img=(ImageView) rowView.findViewById(R.id.list_image);
        holder.tvIllustration = (TextView) rowView.findViewById(R.id.list_description);
        holder.tvTitle.setText(result[position]);
        holder.tvIllustration.setText(result2[position]);
        if(imagePath[position]!= null) {
            File imgFile = new File(imagePath[position]);
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            holder.img.setImageBitmap(myBitmap);
        }

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowInformation.class);
                intent.putExtra("keyName",result[position]);
                context.startActivity(intent);
            }
        });

        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, result[position]+"\n"+result2[position]);
                context.startActivity(Intent.createChooser(sharingIntent,"Share using"));
                return false;
            }
        });

        return rowView;
    }

}
