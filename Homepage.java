package com.traveldiary.porta.traveldiary;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import org.json.JSONObject;
import java.util.Map;

public class Homepage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView travelListView;
    int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        travelListView = (ListView) findViewById(R.id.sampleListView);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Homepage.this, AddNewTravel.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= 23) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        SharedPreferences prefs = getSharedPreferences("appData", MODE_PRIVATE);

        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            i++;
        }
        String[] listItems = new String[i];
        final String[] listItems2 = new String[i];
        final String[] listItems3 = new String[i];
        final String[] listItems4 = new String[i];
        i=0;

        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            listItems[i] = entry.getKey();
            String restoredText = prefs.getString(listItems[i], null);
            if (restoredText != null) {
                try {
                    JSONObject jObj = new JSONObject(restoredText);
                    listItems2[i] = jObj.getString("name");
                    listItems3[i] = jObj.getString("illustration");
                    listItems4[i] = jObj.getString("imagePath");

                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON");
                }

            }
            i++;
        }
        if (listItems.length ==0) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            alertDialogBuilder.setTitle("Insert Travel");

            alertDialogBuilder
                    .setMessage("There are no travel in memory, start by adding a trip")
                    .setCancelable(false)
                    .setNeutralButton("Ok",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        CustomAdapter adapter = new CustomAdapter(this, listItems2,listItems3,listItems4);
        travelListView.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.homepage) {
            Intent intent = new Intent(Homepage.this, Homepage.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Homepage.this, AddNewTravel.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}