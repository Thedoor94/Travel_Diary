package com.traveldiary.porta.traveldiary;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {

    public static String toJSon(Travel travel) {
        try {

            JSONObject jsonObj = new JSONObject();
            jsonObj.put("name", travel.getName());
            jsonObj.put("illustration", travel.getIllustration());
            jsonObj.put("locationStart", travel.getLocationStart());
            jsonObj.put("startDate", travel.getStartDate());
            jsonObj.put("locationClose", travel.getLocationClose());
            jsonObj.put("closeDate", travel.getCloseDate());
            jsonObj.put("imagePath",travel.getImage_path());

            JSONArray jsonArr = new JSONArray();

            for (Stop stop : travel.getStopList()) {
                JSONObject stopObj = new JSONObject();
                stopObj.put("name", stop.getName());
                stopObj.put("latitude", stop.getLatitude());
                stopObj.put("longitude", stop.getLongitude());
                stopObj.put("address", stop.getAddress());
                stopObj.put("shortDescription", stop.getShortDescription());
                jsonArr.put(stopObj);
            }

            jsonObj.put("travel", jsonArr);

            return jsonObj.toString();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;
    }
}