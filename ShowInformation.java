package com.traveldiary.porta.traveldiary;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.json.JSONObject;


public class ShowInformation extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String title_intent;
    private TextView title;
    private TextView illustration;
    private TextView start;
    private TextView close;
    private TextView dateStart;
    private TextView dateClose;
    private Button btnShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_information);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        title = (TextView) findViewById(R.id.textViewTitleTravel);
        illustration = (TextView) findViewById(R.id.textViewDescription);
        start = (TextView) findViewById(R.id.textViewStart);
        close = (TextView) findViewById(R.id.textViewClose);
        dateStart = (TextView) findViewById(R.id.textViewDateStart);
        dateClose = (TextView) findViewById(R.id.textViewDateClose);

        title_intent = getIntent().getStringExtra("keyName");
        SharedPreferences prefs = getSharedPreferences("appData", MODE_PRIVATE);
        String restoredText = prefs.getString(title_intent, null);
        if (restoredText != null) {
            try {
                JSONObject jObj = new JSONObject(restoredText);
                title.setText(jObj.getString("name"));
                illustration.setText(jObj.getString("illustration"));
                start.setText(jObj.getString("locationStart"));
                close.setText(jObj.getString("locationClose"));
                dateStart.setText(jObj.getString("startDate"));
                dateClose.setText(jObj.getString("closeDate"));

            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON");
            }
        }
        btnShow = (Button) findViewById(R.id.button_show_on_map);
        btnShow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowInformation.this, MapsActivity.class);
                intent.putExtra("keyName",title_intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            alertDialogBuilder.setTitle("Delete");

            alertDialogBuilder
                    .setMessage("Are you sure you want to delete this item?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            SharedPreferences settings = getSharedPreferences("appData", Context.MODE_PRIVATE);
                            settings.edit().remove(title_intent).commit();
                            Intent intent = new Intent(ShowInformation.this, Homepage.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {

                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == R.id.homepage) {
            Intent intent = new Intent(ShowInformation.this, Homepage.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(ShowInformation.this, AddNewTravel.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

